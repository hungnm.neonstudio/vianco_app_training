let initState = {
    status: 'null',
    showMessage: false
}

export default (state = initState, action) => {
    switch (action.type) {
        case 'showLoading':
            return { ...state, status: 'showLoading' }
        case 'showMessage':
            return { ...state, status: 'showMessage' }
        default:
            break;
    }
    return state;
}
