import { Dimensions } from 'react-native';

let store = {};

export default api = {
    setStore(_store) {
        store = _store
    },
    colorPrimary() {
        return 'rgb(0, 143, 212)'
    },
    colorWhite() {
        return '#ffffff'
    },
    colorAccent() {
        return 'rgb(236, 29, 36)'
    },
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
}