import React, { Component } from 'react';
import { Text, View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

class Filter extends Component {
    static navigationOptions = {
        title: 'Bộ lọc',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: api.colorPrimary()
        },
        headerLeft: (
            <TouchableOpacity
                onPress={() => Actions.pop()
                }>
                <Icon name='arrow-back' style={{ margin: 16, color: 'white' }} />
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <View style={{flex:1,}}>
                <FlatList
                    data={listFilter}
                    renderItem={({ item }) => <Text style={{padding:20, marginBottom:1, flex:1, backgroundColor:'white'}}>{item}</Text>}>
                </FlatList>
            </View>
        )
    }
}
export default Filter;

const listFilter = [
    'Danh mục',
    'Giảm giá',
    'Giá',
    'Đánh giá',
    'Sản phẩm mới'
]