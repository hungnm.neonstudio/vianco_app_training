import React, { Component } from 'react';
import { Text, View, TextInput, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

export default class ToolBar extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const title = this.props.title;
        const icon = this.props.icon;
        return (
            <View style={{
                height: 56,
                flexDirection: 'row',
                marginBottom: 1
            }}>
                <View style={{
                    flex: 1,
                    borderColor: 'grey',
                    marginRight: 1,
                    backgroundColor: 'white',
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-around'
                }}>
                    <TextInput placeholder='Tìm kiếm'></TextInput>
                    <TouchableOpacity>
                        <Icon style={{ fontSize: 24, color: 'grey' }} name='search' />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={{
                    flex: 1,
                    borderColor: 'grey',
                    backgroundColor: 'white',
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-around'
                }}>
                    <Text style={{ color: 'grey' }}>{title}</Text>
                    <Icon style={{ fontSize: 24, color: 'grey' }} name={icon} />
                </TouchableOpacity>
            </View>
        )
    }
}