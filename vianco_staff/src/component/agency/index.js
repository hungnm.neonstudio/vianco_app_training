import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { Icon } from 'native-base';
import api from '../../api';
import ToolBar from '../../UI/toolbar';

export function agency() {
    let item = address[0];
    return (
        <View style={{ flex: 1 }}>
             <ToolBar title='Khu vực' icon='arrow-down'/>
            <View style={{
                backgroundColor: 'white',
                marginBottom: 1,
                flexDirection: 'row',
                justifyContent: 'space-around',
                padding:16
            }}>
                <View style={{
                    justifyContent: 'space-around',
                }}>
                    <Text>{item.title}</Text>
                    <View style={{ flexDirection: 'row', }}>
                        <Icon name='pin' style={{ fontSize: 24, marginRight: 10 }}></Icon>
                        <Text style={{ alignSelf: 'center' }}>{item.add}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <Icon name='call' style={{ fontSize: 24, marginRight: 10 }}></Icon>
                        <Text style={{ alignSelf: 'center' }}>{item.phone}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <Icon name='archive' style={{ fontSize: 24, marginRight: 10 }}></Icon>
                        <Text style={{ alignSelf: 'center' }}>Đơn hàng gần nhất: {item.last}</Text>
                    </View>
                </View>
                <Icon name='more' style={{ fontSize: 24 }}></Icon>
            </View>
            <TouchableOpacity style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 48
            }}>
                <Text style={{ color: api.colorPrimary() }}>Checkin</Text>
            </TouchableOpacity>
            <FlatList
                data={address}
                renderItem={({ item }) =>
                    <View style={{
                        backgroundColor: 'white',
                        marginBottom: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        height: 72
                    }}>
                        <View style={{
                            justifyContent: 'space-around',
                        }}>
                            <Text>{item.title}</Text>
                            <View style={{ flexDirection: 'row', }}>
                                <Icon name='pin' style={{ fontSize: 24, marginRight: 10 }}></Icon>
                                <Text style={{ alignSelf: 'center' }}>{item.add}</Text>
                            </View>
                        </View>
                        <Icon name='more' style={{ fontSize: 24 }}></Icon>
                    </View>}>
            </FlatList>
        </View>
    )
}

const address = [
    {
        key: 0,
        title: 'Neon studio - Dương Đình Nghệ - Hà Nội',
        add: 'số 9, Dương Đình Nghệ, Cầu Giấy, Hà Nội',
        phone: '01234567890',
        last: '1 ngày trước'
    },
    {
        key: 1,
        title: 'Neon studio - Dương Đình Nghệ - Hà Nội',
        add: 'số 9, Dương Đình Nghệ, Cầu Giấy, Hà Nội',
        phone: '01234567890',
        last: '1 ngày trước'
    },
    {
        key: 2,
        title: 'Neon studio - Dương Đình Nghệ - Hà Nội',
        add: 'số 9, Dương Đình Nghệ, Cầu Giấy, Hà Nội',
        phone: '01234567890',
        last: '1 ngày trước'
    },
    {
        key: 3,
        title: 'Neon studio - Dương Đình Nghệ - Hà Nội',
        add: 'số 9, Dương Đình Nghệ, Cầu Giấy, Hà Nội',
        phone: '01234567890',
        last: '1 ngày trước'
    },
]