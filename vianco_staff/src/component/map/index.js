import React, { Component } from 'react';
import { Text, View, TextInput, Image } from 'react-native';
import {Icon} from 'native-base';
import ToolBar from '../../UI/toolbar';

export function map() {
    return (
        <View style={{ flex: 1 }}>
            <ToolBar title='Khu vực' icon='arrow-down'/>
            <Image
                style={{ 
                    width: '100%',
                    height:'100%'
                 }}
                source={require('../../image/map.jpg')}></Image>
        </View>
    )
}