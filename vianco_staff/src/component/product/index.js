import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, FlatList, Image, } from 'react-native';
import { Icon, Card } from 'native-base';
import { Actions } from 'react-native-router-flux';
import ProductItems from './item';
import api from '../../api';

export function product() {
  return (
    <View style={{ flex: 1, paddingTop:1 }}>
      <ScrollView style={{ flex: 1 }}>
        <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1}}>
          <Text style={{ color: 'grey', }}>Hàng mới về</Text>
          <TouchableOpacity>
            <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          style={{ marginBottom: 3 }}
          horizontal={true}
          data={listProducts}
          renderItem={({ item }) => <TouchableOpacity
            onPress={() => Actions.detail()}>
            <ProductItems />
          </TouchableOpacity>}></FlatList>
        <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1 }}>
          <Text style={{ color: 'grey' }}>Sản phẩm bán chạy</Text>
          <TouchableOpacity>
            <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          style={{ marginBottom: 3 }}
          horizontal={true}
          data={listProducts}
          renderItem={({ item }) =>
            <TouchableOpacity
              onPress={() => Actions.detail()}>
              <ProductItems />
            </TouchableOpacity>}></FlatList>
        <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1 }}>
          <Text style={{ color: 'grey' }}>Giảm giá</Text>
          <TouchableOpacity>
            <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          horizontal={true}
          data={listProducts}
          renderItem={({ item }) =>
            <TouchableOpacity
              onPress={() => Actions.detail()}>
              <ProductItems />
            </TouchableOpacity>}></FlatList>
      </ScrollView>
      <TouchableOpacity style={{
        height: 56,
        backgroundColor: api.colorPrimary(),
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <Text style={{ color: 'white' }}>Xem tất cả sản phẩm</Text>
      </TouchableOpacity>
    </View>
  )
}

const listProducts = [
  {
    key: 0,
    name: 'Đèn Compact',
    url: '../image/led.jpeg'
  },
  {
    key: 1,
    name: 'Đèn LED',
    url: '../image/led.jpeg'
  },
  {
    key: 2,
    name: 'Đèn Tub',
    url: '../image/led.jpeg'
  },
  {
    key: 3,
    name: 'Đèn Compact',
    url: '../image/led.jpeg'
  },
  {
    key: 4,
    name: 'Đèn A',
    url: '../image/led.jpeg'
  },
  {
    key: 5,
    name: 'Đèn B',
    url: '../image/led.jpeg'
  },
]