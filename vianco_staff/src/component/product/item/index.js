import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import api from '../../../api/index';

export default class ProductItems extends Component {
    render() {
        return (
            <View style={styles.cardItems}>
                <View style={{flex:2.5, justifyContent:'center'}}>
                <Image
                    source={require('../../../image/led.jpeg')}
                    style={{ 
                        width: '50%', 
                        height: '100%', 
                        backgroundColor: 'grey', 
                        alignSelf: 'center' }}></Image>
                    </View>
                <View style={{ flex: 2.5, justifyContent: 'center', }}>
                    <Text style={{ 
                        color: 'grey', 
                        fontSize: 12, 
                        fontWeight:'bold' }}>Bóng Compact EU xoắn 24W</Text>
                    <Text style={{ color: 'orange' }}>4.5 ★</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{color:'grey'}}>Giá sỉ: </Text>
                        <Text style={{ fontSize: 12, color: 'red' }}>50.000 vnđ</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{color:'grey'}}>Giá lẻ: </Text>
                        <Text style={{ fontSize: 12, color: 'red' }}>60.000 vnđ</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cardItems: {
        width: (api.width/7) * 3,
        height: api.height/2.7,
        padding: 10,
        marginLeft: 1,
        backgroundColor: 'white',
    }
})