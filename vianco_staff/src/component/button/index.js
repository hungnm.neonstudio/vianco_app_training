import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default Button = {
    home(size, color) {
        return (
            <TouchableOpacity
                onPress={() => Actions.drawerOpen()}>
                <Icon style={{ fontSize: size, color: color, padding: 16 }} name='menu'>
                </Icon>
            </TouchableOpacity>
        )
    },
    search(size, color) {
        return (
            <TouchableOpacity
                onPress={() => Actions.drawerOpen()}>
                <Icon style={{ fontSize: size, color: color, padding: 16 }} name='search'>
                </Icon>
            </TouchableOpacity>
        )
    },
    cart(size, color) {
        return (
            <TouchableOpacity
                onPress={() => Actions.drawerOpen()}>
                <Icon style={{ fontSize: 24, color: 'white', padding: 16 }} name='cart'>
                </Icon>
            </TouchableOpacity>
        )
    }
}