import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { PagerTitleIndicator, IndicatorViewPager } from 'rn-viewpager';
import {info} from './info';
import {agency} from '../../agency';
import {map} from '../../map';

const data = {
    id: '007',
    sum: '1.000.000 vnd',
    discount: '10.000 vnd',
    type: 'Chuyển khoản ngân hàng',
    date: '07/07/2017',
    sendDate: '08/07/2017',

    email: 'design.neonstudio@gmail.com',
    phone: '0912 123 456',
    customer: 'Đại lý',
    address: 'số 9, Dương Đình Nghệ, Cầu Giấy, Hà Nội'
}

export default class OrderDetails extends Component {
    static navigationOptions = {
        title: 'Đơn hàng',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: 'steelblue'
        },
        headerLeft: (
            <TouchableOpacity
                onPress={() => Actions.pop()}>
                <Icon name='arrow-back' style={{ margin: 16, color: 'white' }} />
            </TouchableOpacity>
        )
    }
    _renderTitleIndicator() {
        return (
            <PagerTitleIndicator
                itemStyle={{ backgroundColor: 'white', }}
                itemTextStyle={{ color: 'grey' }}
                selectedBorderStyle={{ backgroundColor: api.colorPrimary() }}
                selectedItemTextStyle={{ color: api.colorPrimary() }}
                titles={['Bản đồ', 'Đơn hàng', 'Đại lý']}>
            </PagerTitleIndicator>
        )
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <IndicatorViewPager
                    style={{flex:1}}
                    indicator={this._renderTitleIndicator()}>
                    {map()}
                    {info(data)}
                    {agency()}
                </IndicatorViewPager>
            </View>
        )
    }
}