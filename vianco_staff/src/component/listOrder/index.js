import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'

export default class ListOrder extends Component {
    static navigationOptions = {
        title: 'Trang chủ',
        headerLeft:
        <View>
            {Button.home(24, 'white')}
        </View>,
        headerTintColor: 'white',
        headerRight:
        <View style={{ flexDirection: 'row' }}>
            {Button.search(24, 'white')}
            {Button.cart(24, 'white')}
        </View>,
        headerStyle: {
            backgroundColor: api.colorPrimary(),
        },
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity
                    style={styles.list}
                >
                    <Text
                        style={styles.txtContent}>
                        Các đơn hàng gần nhất
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.list}
                    onPress={() => {}}
                >
                    <Text
                        style={styles.txtContent}>
                        Đơn hàng đã tiếp nhận
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.list}
                >
                    <Text
                        style={styles.txtContent}>
                        Đơn hàng đang vận chuyển
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.list}
                >
                    <Text
                        style={styles.txtContent}>
                        Đơn hàng thành công
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.list}
                >
                    <Text
                        style={styles.txtContent}>
                        Đơn hàng đã huỷ
                    </Text>
                </TouchableOpacity>
                <View style={{ flex: 1, backgroundColor: 'white' }}></View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    list: {
        marginBottom: 1,
        backgroundColor: 'white',
        padding: 16
    },
    txtContent: {
        color: 'grey'
    }
})