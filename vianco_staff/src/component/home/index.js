import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, FlatList, Image } from 'react-native';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { PagerTitleIndicator, IndicatorViewPager } from 'rn-viewpager';
import { product } from '../product';
import { news } from '../news';
import { sale } from '../sale';
import { promotion } from '../promotion';
import Button from '../../component/button';

class Home extends Component {
  static navigationOptions = {
    title: 'Trang chủ',
    headerLeft:
    <View>
      {Button.home(24, 'white')}
    </View>,
    headerTintColor: 'white',
    headerRight:
    <View style={{ flexDirection: 'row' }}>
      {Button.search(24, 'white')}
      {Button.cart(24, 'white')}
    </View>,
    headerStyle: {
      backgroundColor: api.colorPrimary(),
    },
  }
  _renderTitleIndicator() {
    return (
      <PagerTitleIndicator
        itemStyle={{ backgroundColor: 'white', }}
        itemTextStyle={{ color: 'grey' }}
        selectedBorderStyle={{ backgroundColor: api.colorPrimary() }}
        selectedItemTextStyle={{ color: api.colorPrimary() }}
        titles={['Sản phẩm', 'Hàng mới về', 'Giảm giá', 'Khuyến mại']}>
      </PagerTitleIndicator>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <IndicatorViewPager
          style={{ flex: 1, flexDirection: 'column-reverse' }}
          indicator={this._renderTitleIndicator()}>
          {product()}
          {news()}
          {sale()}
          {promotion()}
        </IndicatorViewPager>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

export default connect()(Home);