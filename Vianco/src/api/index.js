import { Dimensions } from 'react-native';
import { _drawerToggle } from '../component/product/index';
let store={};

export default api = {
    setStore(_store){
        store = _store
    },
    colorPrimary(){
        return 'rgb(0, 143, 212)'
    },
    colorWhite(){
        return '#ffffff'
    },
    colorAccent(){
        return 'rgb(236, 29, 36)'
    },
    showLoading(){ 
        store.dispatch({type:'showLoading'
    })},
    hideLoading(){ store.dispatch({type:'hideLoading'})},
    showMessage(title, content){ store.dispatch({type: 'showMessage', title: title, content: content})},
    hideMessage(){ store.dispatch({type: 'hideMessage'})},
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
}