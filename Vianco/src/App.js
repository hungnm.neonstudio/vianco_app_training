import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Icon } from 'native-base';
import { Router, Modal, Lightbox, Scene, Stack, Drawer, Tabs } from 'react-native-router-flux';
import { Provider } from 'react-redux';

import Login from './component/login';
import LedProduct from './component/product/ledProduct';
import Details from './component/product/productDefails';
import Menu from './component/menu';
import Profile from './component/profile';
import Order from './component/order/index';
import OrderDetails from './component/order/orderDetails';
import Pay from './component/pay';
import PayInfo from './component/pay/payinfo';
import Product from './component/product';
import News from './component/news';
import Sale from './component/sale';
import Promotion from './component/promotion';
import Home from './component/home';
import Filter from './UI/filter';
import store from './reducer';
import UI from './UI/index';
import api from './api';
import Loading from './component/loading/'

export default class App extends Component {
  componentWillMount() {
    api.setStore(store)
  }
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Lightbox key='lightbox'>
            <Scene key={'root'}>
              <Scene key='loading' component={Loading} initial />
              <Scene key='login' component={Login} />
              <Drawer
                key='drawer'
                hideNavBar
                contentComponent={Menu}
                drawerWidth={300}>
                <Scene key='home' component={Home} hideNavBar />
              </Drawer>
            </Scene>

            <Scene key='modal' component={UI} hideNavBar />
          </Lightbox>
        </Router>
      </Provider>
    );
  }
}
