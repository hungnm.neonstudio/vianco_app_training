import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import api from '../../api';
import { Actions } from 'react-native-router-flux';

const width = api.width - 32;
export function login() {
    return (
        <View style={styles.container}>
            <View style={{ flex: 2 }}></View>
            <View style={{ flex: 2, }}>
                <View style={{ marginBottom: 20 }}>
                    <Text style={{ color: 'grey', marginBottom: 5 }}>Số điện thoại</Text>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.input}
                    ></TextInput>
                </View>
                <View>
                    <Text style={{ color: 'grey', marginBottom: 5 }}>Mật khẩu</Text>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.input}
                    ></TextInput>
                </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <TouchableOpacity>
                    <Text style={styles.forget}>Quên mật khẩu</Text>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 3 }}>
                <TouchableOpacity
                    onPress={() => {
                        Actions.drawer()}}
                    style={styles.btn}>
                    <Text style={{ color: 'white' }}>Đăng nhập</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center'
    },
    input: {
        width: width,
        borderWidth: 1,
        borderColor: 'grey',
        padding: 5,
    },
    forget: {
        textDecorationLine: 'underline',
        color: api.colorPrimary()
    },
    btn: {
        alignItems: 'center',
        backgroundColor: api.colorPrimary(),
        borderRadius: 3,
        padding: 10,
        width: width
    }
})