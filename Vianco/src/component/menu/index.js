import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';
import api from '../../api';

export default class Menu extends Component {
    _renderMenuItem(icon, content, tag) {
        return (
            <TouchableOpacity
                onPress={() => {
                    switch(tag){
                        case 'home':
                        return Actions.home();
                        case 'profile':
                        return Actions.profile();
                        case 'news':
                        return Actions.news();
                        case 'logout':
                        return Actions.replace('login');                    }
                }}
                style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', alignItems: 'center', paddingLeft: 20 }}>
                <Icon name={icon} style={{ color: api.colorPrimary(), fontSize: 24, marginRight: 20 }} />
                <Text style={{ color: api.colorPrimary() }}>{content}</Text>
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <Image
                        style={styles.img}
                        source={require('../../image/avatar.jpg')}>
                    </Image>
                    <View>
                        <Text style={{ color: 'white', fontSize: 18 }}>Neon studio</Text>
                        <Text style={{ color: 'white', marginTop: 10 }}>Đại lý</Text>
                    </View>

                </View>
                <View style={{ flex: 2 }}>
                    {this._renderMenuItem('home', 'Trang chủ', 'home')}
                    {this._renderMenuItem('contact', 'Quản lý tài khoản', 'profile')}
                    {this._renderMenuItem('clipboard', 'Đơn hàng của tôi', 'order')}
                    {this._renderMenuItem('notifications', 'Thông báo', 'notifications')}
                    {this._renderMenuItem('paper', 'Tin tức', 'news')}
                    {this._renderMenuItem('log-out', 'Đăng xuất', 'logout')}
                    <View style={{ flex: 2 }}></View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1,
        backgroundColor: api.colorPrimary(),
    },
    img: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginLeft: 32,
        marginRight: 16
    }
})