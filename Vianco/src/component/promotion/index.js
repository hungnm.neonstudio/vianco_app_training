import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Image } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import api from '../../api';

export function promotion() {
    return (
        <View style={styles.container}>
            <FlatList
                style={{padding:16}}
                data={promotions}
                renderItem={({item}) =>
                <View style={{flex:1, backgroundColor:'white', marginBottom:20}}>
                    <Image
                        source={require('../../image/km.jpg')}
                        style={{height: api.height / 3.5, width:'100%'}}
                    ></Image>
                    <Text style={{padding:16, fontWeight:'bold'}}>{item.title}</Text>
                    <Text style={{padding:16}} numberOfLines={3} ellipsizeMode='tail'>{item.content}</Text>
                    <Text style={{color: api.colorPrimary(), textDecorationLine:'underline', alignSelf:'flex-end', marginRight:16, marginBottom:16}}>Xem ngay</Text>
                </View>}
            ></FlatList>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})

const promotions = [
    {
        key:0,
        title: 'Hàng ngàn quà tặng nhân dịp tết Nguyên đán 2018',
        content: 'Nhân dịp tết nguyên đán 2018, Vianco thực hiện chương trình siêu giảm giá, tặng quà với hàng ngàn quà tặng với tổng trị giá lên đên 10 tỉ đồng…',
    },
    {
        key:1,
        title: 'Hàng ngàn quà tặng nhân dịp tết Nguyên đán 2018',
        content: 'Nhân dịp tết nguyên đán 2018, Vianco thực hiện chương trình siêu giảm giá, tặng quà với hàng ngàn quà tặng với tổng trị giá lên đên 10 tỉ đồng…',
    },
    {
        key:2,
        title: 'Hàng ngàn quà tặng nhân dịp tết Nguyên đán 2018',
        content: 'Nhân dịp tết nguyên đán 2018, Vianco thực hiện chương trình siêu giảm giá, tặng quà với hàng ngàn quà tặng với tổng trị giá lên đên 10 tỉ đồng…',
    },
]