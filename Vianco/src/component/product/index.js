import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, FlatList, Image, } from 'react-native';
import { Icon, Card } from 'native-base';
import { Actions } from 'react-native-router-flux';
import ProductItems from './item';

export function product() {
    return (
      <View>
      <ScrollView
        style={{flex:1}}>
        <Text style={{ color: 'grey', padding: 16, backgroundColor: 'white', marginTop: 5, marginBottom: 1 }}>Danh mục sản phẩm</Text>
        <FlatList
          style={{ marginBottom: 3 }}
          data={listProducts}
          horizontal={true}
          renderItem={({ item }) =>
            <View style={{
              backgroundColor: 'white',
              width: 125,
              height: 150,
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
              margin: 1
            }}>
              <Image
                style={{ width: 50, height: 80 }}
                source={require('../../image/led.jpeg')}></Image>
              <Text style={{ fontSize: 12, color: 'grey' }}>{item.name}</Text>
            </View>}>
        </FlatList>
        <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1 }}>
          <Text style={{ color: 'grey', }}>Hàng mới về</Text>
          <TouchableOpacity>
            <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          style={{ marginBottom: 3 }}
          horizontal={true}
          data={listItems}
          renderItem={({ item }) => <TouchableOpacity
            onPress={() => Actions.detail()}>
            <ProductItems />
          </TouchableOpacity>}></FlatList>
        <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1 }}>
          <Text style={{ color: 'grey' }}>Giảm giá</Text>
          <TouchableOpacity>
            <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          horizontal={true}
          data={listItems}
          renderItem={({ item }) =>
            <TouchableOpacity
              onPress={() => Actions.detail()}>
              <ProductItems />
            </TouchableOpacity>}></FlatList>
      </ScrollView>
      </View>
    )
  }
  
  const listItems = [
    {
        key: 1,
        name: 'Product 1'
    },
    {
        key: 2,
        name: 'Product 2'
    },
    {
        key: 3,
        name: 'Product 3'
    }
  ]
  
  const listProducts = [
    {
        key: 0,
        name: 'Đèn Compact',
        url: '../image/led.jpeg'
    },
    {
        key: 1,
        name: 'Đèn LED',
        url: '../image/led.jpeg'
    },
    {
        key: 2,
        name: 'Đèn Tub',
        url: '../image/led.jpeg'
    },
    {
        key: 3,
        name: 'Đèn Compact',
        url: '../image/led.jpeg'
    },
    {
        key: 4,
        name: 'Đèn A',
        url: '../image/led.jpeg'
    },
    {
        key: 5,
        name: 'Đèn B',
        url: '../image/led.jpeg'
    },
  ]