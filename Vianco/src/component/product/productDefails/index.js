import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ScrollView, FlatList, TextInput } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import StarRating from 'react-native-star-rating';

export default class Details extends Component {
    static navigationOptions = {
        title: 'Đèn LED',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: 'steelblue'
        },
        headerLeft: (
            <TouchableOpacity
                onPress={() => Actions.pop()}>
                <Icon name='arrow-back' style={{ margin: 16, color: 'white' }} />
            </TouchableOpacity>
        )
    }
    constructor(props) {
        super(props)
        this.state = {
            starCount: 0,
            nameProduct: 'Bóng Compact xoắn 24W',
            price: '50.000 vnđ',
            sale: '47.000 vnđ',
            quantity: 2,
            sum: '100.000 vnđ,',
            light: 'Trắng',
            type: 'Xoáy/Cài',
            from: 'Việt Nam',
            model: 'CFL Xoắn 24W',
            lumen: '1025 Lm',
            ra: '> 80, 6500K',
            life: '8000h',
            efficiency: '60.28lm/W',
            ac: '150 - 240V, 50Hz',
            listImage: [
                'https://img.websosanh.vn/v2/users/root_product/images/FXJFkn5JEdmI.jpg?width=200&compress=85',
                'https://img.websosanh.vn/v2/users/root_product/images/FXJFkn5JEdmI.jpg?width=200&compress=85',
                'https://img.websosanh.vn/v2/users/root_product/images/FXJFkn5JEdmI.jpg?width=200&compress=85',
            ],
            content: 'Bóng đèn Compact EUROSUPER - EUROSTAR được sản xuất tại Việt Nam với dây chuyền hiện đại theo tiêu chuẩn công nghệ Châu Âu được người tiêu dùng bình chọn.',
            tut: [
                'Lắp bóng đèn vào đuôi đèn. Lưu ý không lắp bóng đèn khi công tắc điện vào đèn đang bật để an toàn khi lắp bóng',
                'Không dùng với công tắc điều chỉnh độ sáng',
                'Không lắp bóng đèn ở khu vực ngoài trời có tiếp xúc' + '\r\n' + 'trực tiếp với nước và độ ẩm cao.',
                'Lắp bóng đèn xa nguồn nhiệt để bảo vệ linh kiện điện tử.'
            ]
        }
    }
    _onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }
    _renderRows(header, content) {
        return (
            <View style={{ flexDirection: 'row', padding: 5 }}>
                <Text style={{ color: 'grey' }}>• {header}:  {content}</Text>
            </View>
        )
    }
    render() {
        const { tut, nameProduct, price, type, quantity, sum, life, light, from, model, lumen, ra, efficiency, ac } = this.state;
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{
                    height: 250,
                    width: '100%',
                    justifyContent: 'center',
                    backgroundColor: 'white',
                    marginBottom: 1
                }}>
                    <Image
                        resizeMode='contain'
                        style={{ height: 175 }}
                        source={{ uri: 'https://img.websosanh.vn/v2/users/root_product/images/FXJFkn5JEdmI.jpg?width=200&compress=85', }}></Image>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    padding: 16,
                    marginBottom: 5
                }}>
                    <Text style={{ fontSize: 16 }}>{this.state.nameProduct}</Text>
                    <Text style={{ color: 'orange', paddingTop: 10, paddingBottom: 10 }}>4.5 ★</Text>
                    <Text style={{ color: 'red', paddingBottom: 10 }}>{this.state.sale}</Text>
                    <Text style={{
                        color: 'grey',
                        textDecorationLine: 'line-through',
                        textDecorationColor: 'grey'
                    }}>{this.state.price}</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    padding: 16,
                    marginBottom: 1
                }}>
                    <Text style={styles.header}>Thông tin sản phẩm</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    padding: 16,
                    marginBottom: 5
                }}>
                    {this._renderRows('Tên sản phẩm', nameProduct)}
                    {this._renderRows('Ánh sáng', light)}
                    {this._renderRows('Đui/xoáy cài', type)}
                    {this._renderRows('Xuất xứ', from)}
                    {this._renderRows('MODEL', model)}
                    {this._renderRows('Lumen', lumen)}
                    {this._renderRows('Ra', ra)}
                    {this._renderRows('Tuổi thọ', life)}
                    {this._renderRows('Hiệu suất năng lượng', efficiency)}
                    {this._renderRows('AC', ac)}
                    <Text style={{ marginTop: 20 }}>{this.state.content}</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    padding: 16,
                    marginBottom: 1
                }}>
                    <Text style={styles.header}>Hướng dẫn sử dụng</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    padding: 16,
                    marginBottom: 5
                }}>
                    {tut.map((value, index, tut) => {
                        return this._renderRows('', value)
                    })}
                </View>
                <View style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    padding: 16,
                    marginBottom: 1
                }}>
                    <Text style={styles.header}>Đánh giá nhận xét</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 5
                }}>
                    <Text style={styles.header}>Đánh giá</Text>
                    <View style={{ width: 125, marginTop: 10, marginBottom: 10 }}>
                        <StarRating
                            disabled={false}
                            maxStars={5}
                            starSize={20}
                            halfStarEnabled={true}
                            rating={this.state.starCount}
                            starColor='yellow'
                            emptyStarColor='yellow'
                            selectedStar={(rating) => this._onStarRatingPress(rating)}
                        />
                    </View>
                    <Text style={styles.header}>Nhận xét</Text>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={{ borderBottomColor: 'grey', borderBottomWidth: 0.5, padding: 10 }}
                        placeholder='Tên của bạn'>
                    </TextInput>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={{ borderBottomColor: 'grey', borderBottomWidth: 0.5, padding: 10 }}
                        placeholder='Tựa đề bài viết'>
                    </TextInput>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={{ borderBottomColor: 'grey', borderBottomWidth: 0.5, padding: 10 }}
                        placeholder='Viết nhận xét'>
                    </TextInput>
                    <View style={{ height: 125 }}>

                    </View>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1
                }}>
                    <Text style={styles.header}>Sản phẩm đã xem</Text>
                </View>
                <FlatList>

                </FlatList>
                <TouchableOpacity 
                    onPress={() => Actions.orderDetails()}
                    style={{ backgroundColor: 'red', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontSize: 16, padding: 16 }}>Thêm vào giỏ hàng</Text>
                </TouchableOpacity>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        color: "grey"
    }
})