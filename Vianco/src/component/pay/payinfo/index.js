import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import api from '../../../api/index';
import { Actions } from 'react-native-router-flux';

export default class PayInfo extends Component {
    _renderRows(header) {
        const width = api.width - 32;
        return (
            <View>
                <Text style={{ marginBottom: 5 }}>{header}</Text>
                <TextInput 
                    underlineColorAndroid='transparent'
                    style={{
                    width: width,
                    height: 40,
                    borderWidth: 0.1,
                    borderRadius: 20,
                    padding:10
                }}></TextInput>
            </View>
        )
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'space-around', padding: 16 }}>
                {this._renderRows('Email')}
                {this._renderRows('Tên')}
                {this._renderRows('Tỉnh/thành')}
                {this._renderRows('Quận/huyện')}
                {this._renderRows('Số nhà')}
                {this._renderRows('Số điện thoại')}
                <Text>Phương thức thanh toán</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <Text>Ship COD</Text>
                    <Text>Chuyển khoản</Text>
                </View>
                <TouchableOpacity
                    onPress={()=> Actions.order()}
                    style={{
                        alignSelf: 'center', 
                        width: api.width - 32,
                        backgroundColor: 'steelblue',
                        alignItems:'center',
                        padding:10,
                        borderRadius:20,
                        shadowColor:'grey',
                        shadowOffset:{
                            width:1,
                            height:0
                        },
                        elevation:2,
                        shadowOpacity:0.8,
                        shadowRadius:2
                    }}>
                    <Text style={{
                        color: 'white'
                    }}>Thanh toán</Text>
                </TouchableOpacity>
            </View>
        )
    }
}