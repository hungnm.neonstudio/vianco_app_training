import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native';
import api from '../api/index';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

export default class MSG extends Component {
  
  componentDidMount = () => {
    console.log("LOADING", this.props.showLoading);
  }
  
  render() {
    let { showLoading } = this.props;
    console.log(showLoading)
    return (
      <Modal
        transparent={true}
        animationType='fade'
        onRequestClose={() => { }}
        visible={showLoading}>
        <View style={styles.container}>
          <ActivityIndicator size='large' style={{ alignSelf: 'center' }} />
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1
  },
})
