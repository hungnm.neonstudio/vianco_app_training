import request from './request';
import config from './config';
export default dataService = {
    // loginw
    // ham login bao gồm pas vs phone token

    login(phone, password, firebaseToken) {
        let url = 'customer/login'
        let data = {
            phone: phone,
            password: password,
            firebaseToken: firebaseToken
        }
        return request.post(url, data, '123')
    },
    register(phone, password,name,email,type) {
        let url ='customer/register'
        let data ={
            phone:phone,
            name:name,
            password:password,
            email:email,
            type: type
        }
        return request.post(url,data)
    },
    listProduct( start,number,type,category){
        let url = 'customer/listProduct'
        let data = {
            start,number,type,category
        }
        return request.post(url, data);
    },
    listCategory(){
        let url ='customer/listCategory'
        let data ={
        }
        return request.post(url,data);
    },
    listDiscount(start,number,type,category){
        let url = 'customer/listProduct'
        let data = {
            start,number,type,category
        }
        return request.post(url, data);
    }
}