import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid, ScrollView, FlatList, TextInput
} from 'react-native';
import Draw from 'react-native-drawer';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';
import { SearchBar } from 'react-native-elements';
import CartItem from '../carts/items';

import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class Carts extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>
                    <TouchableOpacity
                        onPress={() => { Actions.drawerOpen() }}
                    >
                        <Image source={require('../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                    </TouchableOpacity>
                    <Text style={{
                        flex: 1,
                        fontSize: 15, color: '#fff', flex: 1,
                        textAlign: 'center'
                    }}>
                        Giỏ hàng
                    </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../image/search.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { Actions.carts() }} >
                            <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../image/cart.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <ScrollView>
                        {/* danh sach san pham */}
                        <View>
                            <FlatList
                                horizontal={false} data={listProduct}
                                style={{}}
                                renderItem={({ item }) =>
                                    <View style={{ flexDirection: 'row', height: 125, justifyContent: 'center', padding: 20, borderBottomColor: 'grey', borderBottomWidth: 2 }}>
                                        <View style={{ flex: 1 }}><Image source={require('../../image/led.jpeg')} style={{ width: 75, height: 100 }} /></View>
                                        <View style={{ flex: 3 }}>
                                            <Text>{item.tensp}</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text>Giá sỉ : </Text>
                                                <Text>{item.giadaily}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text>Giá lẻ : </Text>
                                                <Text>{item.giasile}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text>Số lượng : </Text>
                                                <TouchableOpacity>
                                                    <Icon name='md-add-circle' style={{ fontSize: 20, color: 'red' }} />
                                                </TouchableOpacity>
                                                <Text> {item.soluong} </Text>
                                                <TouchableOpacity>
                                                    <Icon name='md-remove-circle' style={{ fontSize: 20, color: 'red' }} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                }
                            />
                        </View>

                    </ScrollView>
                    {/* phan button */}
                    <View style={{ borderColor: 'grey', borderWidth: 1 }}>
                        <View style={{ flexDirection: 'row', height: 50, justifyContent: 'center', alignItems: 'center', borderBottomColor: 'grey', borderBottomWidth: 1 }}>
                            <TextInput placeholder='Nhập mã khuyến mãi' style={{ flex: 3 }} underlineColorAndroid="transparent" />
                            <View style={{ flex: 2, }}>
                                <TouchableOpacity style={{ height: 30, width: 80, backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: 'white' }}>Gửi</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', height: 70, justifyContent: 'center' }}>
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text>Tổng : </Text>
                                    <Text> 1.500.000 vnđ</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text>Giảm : </Text>
                                    <Text> 500.000 vnđ</Text>
                                </View>
                            </View>
                            <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'red' }} >
                                <Text style={{ color: 'white' }}>Đặt mua</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

            </View>
        );
    }

}
const styles = StyleSheet.create({
    btn: {
        height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1
    },
    txtTou: {
        marginLeft: 30,

    }
});
const listProduct = [
    {
        key: 0,
        tensp: 'Bóng đèn compact 45W',
        giasile: '60.000',
        giadaily: '55.000',
        soluong: '2'
    },
    {
        key: 1,
        tensp: 'Bóng đèn compact 45W',
        giasile: '60.000',
        giadaily: '55.000',
        soluong: '2'
    },
    {
        key: 2,
        tensp: 'Bóng đèn compact 45W',
        giasile: '60.000',
        giadaily: '55.000',
        soluong: '2'
    },
    {
        key: 3,
        tensp: 'Bóng đèn compact 45W',
        giasile: '60.000',
        giadaily: '55.000',
        soluong: '2'
    },



]
