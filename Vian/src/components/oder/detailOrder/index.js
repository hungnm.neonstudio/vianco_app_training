import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid, ScrollView, FlatList, TextInput
} from 'react-native';
import Draw from 'react-native-drawer';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons'
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class DetailOrder extends Component {
    // filterSeach(text){
    //     // alert('chưa hoàn thiện')
    // }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>
                    <TouchableOpacity
                        onPress={() => { Actions.drawerOpen() }}
                    >
                        <Image source={require('../../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                    </TouchableOpacity>
                    <Text style={{
                        flex: 1,
                        fontSize: 15, color: '#fff', flex: 1,
                        textAlign: 'center'
                    }}>
                        Chi tiết đơn hàng
                </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../../image/search.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { Actions.carts() }} >
                            <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../../image/cart.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <ScrollView>
                        {/* thong tin don hang */}
                        <View>
                            <View style={{ borderBottomColor: '#767676', borderBottomWidth: 1, height: 40, justifyContent: 'center', paddingLeft: 20 }}>
                                <Text>
                                    Thông tin đơn hàng
                                 </Text>
                            </View>
                            <View style={{ padding: 20 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={{ color: 'grey', fontWeight: 'bold', fontSize: 13 }}>Mã đơn hàng : </Text>
                                    <Text style={styles.txtPrice}>007</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={styles.txtTitle}>Tổng hóa đơn : </Text>
                                    <Text style={styles.txtPrice}>1.0000.00 đ</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={styles.txtTitle}>Khuyến mãi :  </Text>
                                    <Text style={styles.txtPrice}>10.000 đ</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={styles.txtTitle}>Phương thức thanh toán : </Text>
                                    <Text style={styles.txtPrice}>Chuyển khoản ngân hàng</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={styles.txtTitle}>Ngày đặt : </Text>
                                    <Text style={styles.txtPrice}>07/11/2017</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={styles.txtTitle}>Ngày nhận : </Text>
                                    <Text style={styles.txtPrice}>08/11/2017</Text>
                                </View>
                            </View>
                        </View>
                        {/* tien */}
                        <View style={{  }}>
                            <View style={{ padding: 20 ,flexDirection: 'row', height: 40, borderColor: '#767676', borderWidth: 1, alignItems: 'center' }}>
                                <Text style={{ flex: 1,fontSize:13,fontWeight:'bold'}}>Sản phẩm</Text>
                                <Text style={{ flex: 1,fontSize:13,fontWeight:'bold' }}>Tiền</Text>
                            </View>
                            <View style={{ flexDirection: 'row',padding:20 ,alignItems:'center',height:25}}>
                                <View style={{ flex: 1,flexDirection:'row' }} >
                                    <Text style={styles.txtPrice}>Bóng đèn </Text>
                                    <Text style={{color:'red',fontSize:13}}> x2</Text>
                                </View>
                                <View style={{ flex: 1, }} >
                                    <Text style={styles.txtPrice}> 20.000 vnđ</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row',padding:20 ,alignItems:'center',height:25}}>
                                <View style={{ flex: 1,flexDirection:'row' }} >
                                    <Text style={styles.txtPrice}>Bóng đèn </Text>
                                    <Text style={{color:'red',fontSize:13}}> x2</Text>
                                </View>
                                <View style={{ flex: 1, }} >
                                    <Text style={styles.txtPrice}> 20.000 vnđ</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row',padding:20 ,alignItems:'center',height:25}}>
                                <View style={{ flex: 1,flexDirection:'row' }} >
                                    <Text style={styles.txtPrice}>Bóng đèn </Text>
                                    <Text style={{color:'red',fontSize:13}}> x2</Text>
                                </View>
                                <View style={{ flex: 1, }} >
                                    <Text style={styles.txtPrice}> 20.000 vnđ</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row',padding:20 ,alignItems:'center',height:25}}>
                                <View style={{ flex: 1,flexDirection:'row' }} >
                                    <Text style={styles.txtPrice}>Bóng đèn </Text>
                                    <Text style={{color:'red',fontSize:13}}> x2</Text>
                                </View>
                                <View style={{ flex: 1, }} >
                                    <Text style={styles.txtPrice}> 20.000 vnđ</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row',padding:20 ,alignItems:'center',height:25}}>
                                <View style={{ flex: 1,flexDirection:'row' }} >
                                    <Text style={styles.txtPrice}>Bóng đèn </Text>
                                    <Text style={{color:'red',fontSize:13}}> x2</Text>
                                </View>
                                <View style={{ flex: 1, }} >
                                    <Text style={styles.txtPrice}> 20.000 vnđ</Text>
                                </View>
                            </View>
                        </View>
                        {/* thông tin khach hang */}
                        <View style={{ padding: 20 ,flexDirection: 'row', height: 40, borderColor: '#767676', borderWidth: 1, alignItems: 'center'}}>
                            <Text>Thông tin khách hàng</Text>
                        </View>
                        <View style={{ padding: 20 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={{ color: 'grey', fontWeight: 'bold', fontSize: 13 }}>Email : </Text>
                                    <Text style={styles.txtPrice}>Hoihoangna@gmail.com</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={styles.txtTitle}>Số điện thoại : </Text>
                                    <Text style={styles.txtPrice}>0965.0965.02</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={styles.txtTitle}>Khách hàng :  </Text>
                                    <Text style={styles.txtPrice}>Đại lý</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                                    <Icon name='ios-radio-button-on' style={{ fontSize: 7, paddingRight: 5 }} />
                                    <Text style={styles.txtTitle}>Địa chỉ : </Text>
                                    <Text style={styles.txtPrice}>Số 9, Dương Đình Nghệ, Cầu Giấy, HN</Text>
                                </View>
                            </View>
                    </ScrollView>
                </View>

            </View>
        );
    }

}
const styles = StyleSheet.create({
    btn: {
        height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1
    },
    txtTou: {
        marginLeft: 30,

    },
    styClum: {

    },
    txtTitle: {
        color: 'grey', fontWeight: 'bold', fontSize: 13
    },
    txtPrice: {
        fontSize: 13
    }
});
