import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import dataService from '../../api/dataService'
import api from '../../api';

export default class Login extends Component {
    _goLogin() {
        if(!this.state.phone) return alert('ban hay nhap vao sdt');
        dataService.login(this.state.phone, this.state.password, 'agasgada')
            
            // then là sờ vơ trả về
            .then(rs => {
                // alert(JSON.stringify(rs))
                if(rs.err == 0){
                    api.setToken(rs.token.token);
                    Actions.home()
                }
                if(rs.err != 0){
                    alert('Thông tin đăng nhập không đúng')
                }
            })
    }
    constructor(props) {
        super(props);
        this.state = {
            phone: '',
            password:''

        }
    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 4, marginTop: 100, margin: 20 }}>
                    <View>
                        <Text style={{ fontWeight: 'bold', fontSize: 13, marginBottom: 2 }}>Số điện thoại</Text>
                        <TextInput underlineColorAndroid="transparent" style={styles.txtInput}
                         onChangeText={(phone) => this.setState({ phone })}
                         value={this.state.phone}
                        >
                        </TextInput>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 13, marginBottom: 2 }}>Mật khẩu</Text>
                        <TextInput underlineColorAndroid="transparent" style={styles.txtInput}
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                        ></TextInput>
                    </View>
                    <View style={{ justifyContent: 'center', marginTop: 20 }}>
                        <TouchableOpacity style={{}}>
                            <Text style={{ color: 'steelblue', textAlign: 'center', textDecorationLine: 'underline' }}>
                                Quên mất khẩu ?
                    </Text>
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity
                        onPress={() => {
                            this._goLogin()
                        }}
                        style={{ backgroundColor: 'steelblue', marginTop: 30, height: 35, justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', color: 'white' }}>Đăng nhập</Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    txtInput: {
        shadowOpacity: 1,
        shadowColor: 'black',
        fontSize: 13, height: 35, flexDirection: 'row', borderWidth: 1, borderColor: 'grey',

    },
    txtTitle: {
        fontWeight: 'bold', fontSize: 13,
    },
})
// Actions.home(
//     type='reset'
// );