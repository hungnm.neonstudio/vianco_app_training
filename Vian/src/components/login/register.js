import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import dataService from '../../api/dataService'
export default class Register extends Component {
    _goRegister() {
        //alert(this.state.email);
        if (this.state.password != this.state.repass) {
            return alert(' mat khau khong khop')
        };
        if (!this.state.phone) return alert('Mời nhập số điện thoại ');
        if (!this.state.name) return alert('Mời bạn nhập lại tên');
        if (!this.state.email) return alert('Mời bạn nhập email');
        if (!this.state.password) return alert('Mời bạn nhập pass');
        if (!this.state.repass) return alert('Nhập lại pass');
        dataService.register( this.state.phone, this.state.password,this.state.name,this.state.email, 
             this.state.type)
            .then(rs => {
                alert(JSON.stringify(rs))
            })
    }
    constructor(props) {
        super(props)
        this.state = {
            type: 'guest',
            phone: '',
            name: '',
            email: '',
            password: '',
            repass: '',


        }
    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 9, justifyContent: 'center', margin: 20 }}>
                    <View>
                        <Text style={{ fontSize: 13, color: 'black' }}>Số điện thoại</Text>
                        <TextInput underlineColorAndroid="transparent" style={styles.txtInput}
                            onChangeText={(phone) => this.setState({ phone })}
                            value={this.state.phone}
                        ></TextInput>
                    </View>
                    <View>
                        <Text style={{ fontSize: 13, color: 'black' }}>Họ tên </Text>
                        <TextInput underlineColorAndroid="transparent" style={styles.txtInput}
                            onChangeText={(name) => this.setState({ name })}
                            value={this.state.name}
                        ></TextInput>
                    </View>
                    <View>
                        <Text style={{ fontSize: 13, color: 'black' }}>Email</Text>
                        <TextInput underlineColorAndroid="transparent" style={styles.txtInput}
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                        ></TextInput>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={{ fontSize: 13, color: 'black' }}>Mật khẩu</Text>
                        <TextInput underlineColorAndroid="transparent" style={styles.txtInput}
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                        ></TextInput>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={{ fontSize: 13, color: 'black' }}>Nhập lại mật khẩu</Text>
                        <TextInput underlineColorAndroid="transparent" style={styles.txtInput}
                            onChangeText={(repass) => this.setState({ repass })}
                            value={this.state.repass}
                        ></TextInput>
                    </View>
                    <View>
                        <Text style={{ textAlign: 'center', marginTop: 15, color: 'black' }}>Lựa chọn người sử dụng</Text>
                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                            <TouchableOpacity style={{
                                backgroundColor: this.state.type == 'guest' ? 'steelblue' : 'white',
                                borderRadius: 15,
                                paddingLeft: 20,
                                paddingRight: 20,
                                paddingTop: 5,
                                paddingBottom: 5,
                                margin: 3
                            }}
                                onPress={() => {
                                    this.setState({ type: 'guest' })
                                }}
                            >
                                <Text style={{ color: 'white', fontSize: 12 }} >Khách hàng lẻ</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{
                                backgroundColor: this.state.type == 'agency' ? 'steelblue' : 'white',
                                borderRadius: 15,
                                paddingLeft: 20,
                                paddingRight: 20,
                                paddingTop: 5,
                                paddingBottom: 5,
                                margin: 3,
                                borderColor: 'grey'
                            }}
                                onPress={() => {
                                    this.setState({ type: 'agency' })
                                }}
                            >
                                <Text style={{ color: 'white', fontSize: 12 }} >Khách hàng đại lý</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => {
                        this._goRegister()
                    }}
                        style={{ backgroundColor: 'steelblue', marginTop: 30, height: 35, justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Đăng ký</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    toolbar: {
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        flex: 1,
        backgroundColor: 'steelblue',
        flexDirection: 'row'
    },
    txtInput: {
        shadowOpacity: 1,
        shadowColor: 'black',
        fontSize: 13, height: 35, flexDirection: 'row', borderWidth: 1, borderColor: 'grey',

    },
    txtTitle: {
        fontWeight: 'bold', fontSize: 13,
    },
})