import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, ScrollView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';
import api from '../../api/index';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export function promotion() {
    return (
        <View style={{ flex: 1, padding: 20 }}>
            <ScrollView>
                <TouchableOpacity>
                    <Image source={require('../../image/khuyenmai.png')} style={{ height: 200, width: '100%' }} />
                    <Text style={{ color: 'black', fontWeight: 'bold', alignItems: 'center' }}>Hàng ngàn quà tặng hấp dẫn trong tháng 11</Text>
                    <Text>
                        Nhân dịp tết nguyên đán 2018, Vianco thực hiện
                   chương trình siêu giảm giá, tặng quà với hàng ngàn
                   quà tặng với tổng trị giá lên đên 10 tỉ đồng…
                     </Text>
                    <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop:20}}>
                    <Image source={require('../../image/khuyenmai.png')} style={{ height: 200, width: '100%' }} />
                    <Text style={{ color: 'black', fontWeight: 'bold', alignItems: 'center' }}>Hàng ngàn quà tặng hấp dẫn trong tháng 11</Text>
                    <Text>
                        Nhân dịp tết nguyên đán 2018, Vianco thực hiện
                   chương trình siêu giảm giá, tặng quà với hàng ngàn
                   quà tặng với tổng trị giá lên đên 10 tỉ đồng…
                        </Text>
                    <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
}

