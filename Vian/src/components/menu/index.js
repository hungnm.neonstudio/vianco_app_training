import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';

export default class Menu extends Component {
    state = {}
    render() {
        return (
            <View style={{ flex: 1, }}>
                <View style={{ flex: 3, backgroundColor: '#048fd2', alignItems: 'flex-start', }}>
                    <View style={{ padding: 30, flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ height: 100, width: 100, borderRadius: 50, }} source={require('../../image/tungson.png')} />
                        <View style={{ marginLeft: 15 }}>
                            <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15 }}>Neon Studio</Text>
                            <Text style={{ color: 'white', fontSize: 10 }} >Đại lý</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 6, backgroundColor: 'white', padding: 20 }}>
                    <TouchableOpacity style={{ height: 40, alignItems: 'center', flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#f6f6f6', }} onPress={() => { Actions.push('home'); }}>
                        <Icon name='ios-home-outline' style={{ fontSize: 20, color: '#048fd2', marginRight: 10 }} />
                        <Text style={{ color: '#048fd2', fontSize: 15, }}>Trang chủ</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => { Actions.account(); }}>
                        <Icon name='ios-contact-outline' style={{ fontSize: 20, color: '#048fd2', marginRight: 10, }} />
                        <Text style={{ color: '#048fd2', fontSize: 15, textAlign: 'center' }} >Quản lý tài khoản</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} >
                        <Icon name='ios-card' style={{ fontSize: 20, color: '#048fd2', marginRight: 10 }} />
                        <Text style={{ color: '#048fd2', fontSize: 15 }} >Đơn phải giao</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => { Actions.maps(); }}>
                        <Icon name='ios-pin-outline' style={{ fontSize: 20, color: '#048fd2', marginRight: 10 }} />
                        <Text style={{ color: '#048fd2', fontSize: 15 }} >Bản đồ</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn}>
                        <Icon name='ios-notifications-outline' style={{ fontSize: 20, color: '#048fd2', marginRight: 10 }} />
                        <Text style={{ color: '#048fd2', fontSize: 15 }} >Thông báo</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => { Actions.news(); }}>
                        <Icon name='md-bookmarks' style={{ fontSize: 20, color: '#048fd2', marginRight: 10 }} />
                        <Text style={{ color: '#048fd2', fontSize: 15 }} >Tin tức</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn}>
                        <Icon name='md-log-out' style={{ fontSize: 20, color: '#048fd2', marginRight: 10 }} />
                        <Text style={{ color: '#048fd2', fontSize: 15 }} >Đăng xuất </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    btn: {
        height: 40,
        alignItems: 'flex-start',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#f6f6f6', alignItems: 'center',
    }
})