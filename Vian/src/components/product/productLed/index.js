import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid, ScrollView, FlatList,
} from 'react-native';
import ProductItems from '../item/index';
import Draw from 'react-native-drawer';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons'
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class ProductLed extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>
                    <TouchableOpacity
                        onPress={() => { Actions.drawerOpen() }}
                    >
                        <Image source={require('../../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                    </TouchableOpacity>
                    <Text style={{
                        flex: 1,
                        fontSize: 15, color: '#fff', flex: 1,
                        textAlign: 'center'
                    }}>
                        LED
                </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../../image/search.png')} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { Actions.carts() }} >
                            <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../../image/cart.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <ScrollView>
                        <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1 }}>
                            <Text style={{ color: 'grey', }}>Hàng mới về</Text>
                            <TouchableOpacity>
                                <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
                            </TouchableOpacity>

                        </View>
                        <FlatList
                            style={{ marginBottom: 3 }}
                            horizontal={true}
                            data={listItems}
                            renderItem={({ item }) =>
                                <TouchableOpacity onPress={() => Actions.detail()}>
                                    <ProductItems />
                                </TouchableOpacity>}></FlatList>
                        {/* hang ban chay */}
                        <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1 }}>
                            <Text style={{ color: 'grey' }}>Hàng bán chạy nhất</Text>
                            <TouchableOpacity>
                                <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            horizontal={true}
                            data={listItems}

                            renderItem={({ item }) =>

                                <TouchableOpacity
                                    onPress={() => Actions.detail()}>
                                    <ProductItems />
                                </TouchableOpacity>}>
                        </FlatList>
                        {/* Hàng giảm giá */}
                        <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1 }}>
                            <Text style={{ color: 'grey' }}>Giảm giá</Text>
                            <TouchableOpacity>
                                <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            horizontal={true}
                            data={listItems}
                            renderItem={({ item }) =>
                                <TouchableOpacity
                                    onPress={() => Actions.detail()}>
                                    <ProductItems />
                                </TouchableOpacity>}></FlatList>
                    </ScrollView>
                </View>

            </View>
        );
    }

}
const listItems = [
    {
        key: 1,
        name: 'Product 1'
    },
    {
        key: 2,
        name: 'Product 2'
    },
    {
        key: 3,
        name: 'Product 3'
    }
]
const listProducts = [
    {
        key: 0,
        name: 'Đèn Compact',
        url: '../image/led.jpeg'
    },
    {
        key: 1,
        name: 'Đèn LED',
        url: '../image/led.jpeg'
    },
    {
        key: 2,
        name: 'Đèn Tub',
        url: '../image/led.jpeg'
    },
    {
        key: 3,
        name: 'Đèn Compact',
        url: '../image/led.jpeg'
    },
    {
        key: 4,
        name: 'Đèn A',
        url: '../image/led.jpeg'
    },
    {
        key: 5,
        name: 'Đèn B',
        url: '../image/led.jpeg'
    },
]
const styles = StyleSheet.create({
    btn: {
        height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1
    },
    txtTou: {
        marginLeft: 30,

    }
});
