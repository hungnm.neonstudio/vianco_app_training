import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid, FlatList,
} from 'react-native';

import Draw from 'react-native-drawer';
// import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';

import api from '../../../api/index';
import { product } from '../../product';
import { sale } from '../../sale';
import { productNews } from '../../productNews';
import { promotion } from '../../promotion';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class InforAccount extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>
                    <View style={{}} >
                        <TouchableOpacity
                            onPress={() => { Actions.drawerOpen() }}
                        >
                            <Image source={require('../../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, marginTop: 15 }} >
                        <Text style={{
                            fontSize: 15, color: '#fff', flex: 1,
                            textAlign: 'center'
                        }}>
                            Tài khoản
                    </Text>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../../image/search.png')} />
                        </TouchableOpacity>

                        <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../../image/cart.png')} />
                    </View>


                </View>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 3, backgroundColor: '#048fd2', alignItems: 'center', justifyContent: 'center', }}>
                        <View style={{ padding: 30, flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{}}>
                                <Image style={{ height: 100, width: 100, borderRadius: 50 }} source={require('../../../image/tungson.png')} >

                                </Image>
                                <TouchableOpacity style={{ position: 'absolute', zIndex: 39 }}>
                                    <Image style={{ marginTop: 70, marginLeft: 70 }} source={require('../../../image/camera.png')} />
                                </TouchableOpacity>
                            </View>




                            <View style={{ marginLeft: 15 }}>
                                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 15 }}>Neon Studio</Text>
                                <Text style={{ color: 'white', fontSize: 10 }} >Đại lý</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 7 }}>
                        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ marginLeft: 30 }}>Giới tính : </Text>
                            <Text>Nam</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ marginLeft: 30 }}>Tuổi : </Text>
                            <Text>23</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ marginLeft: 30 }} >Tỉnh thành  : </Text>
                            <Text>Hà Nội</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ marginLeft: 30 }}>Quận huyện: </Text>
                            <Text>Cầu Giấy</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ marginLeft: 30 }}>Số nhà: </Text>
                            <Text>69</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ marginLeft: 30 }}>Số điện thoại: </Text>
                            <Text>0965096502</Text>
                        </View>
                        <View style={{ flex: 1, backgroundColor: '#048fd2', }}>
                            <TouchableOpacity style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} >
                                <Text style={{ textAlign: 'center', color: 'white', }}>Cập nhật thông tin </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

            </View>
        );
    }
}
