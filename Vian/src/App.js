import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Scene, Router, Stack, Modal, Drawer } from 'react-native-router-flux';
// import Drawer from 'react-native-drawer'
import { Actions } from 'react-native-router-flux';
import { Provider } from 'react-redux';
import Sign from './components/login/index';
import api from './api/index';
import Home from './components/home/index';
import Menu from './components/menu/index';
import Product from './components/product/index';
import Sale from './components/sale';
import Detail from './components/product/productDetail';
import Account from './components/account/index';
import InforAccount from './components/account/inforAccount';
import News from './components/news';
import Oder from './components/oder';
import Loading from './components/loading/index';
import ProductLed from './components/product/productLed';
import RecentOrder from './components/oder/recentOrders';
import ReceivedOrder from './components/oder/receivedOrders';
import Maps from './components/maps/index';
import ShippedOrder from './components/oder/shippedOrder';
import DetailOrder from './components/oder/detailOrder';
import Carts from './components/carts/index';
export default class App extends Component {
  render() {
    return (

      <Router>
        <Modal key='root' hideNavBar>

          {/* <Scene key='loading' component={Loading}  /> */}
          <Drawer
            contentComponent={Menu}
            drawerWidth={300}
            hideNavBar
          >
            <Stack key="root" hideNavBar>

              <Scene key="sign" component={Sign}  />
              <Scene key="home" component={Home} initial />
              <Scene key="detail" component={Detail} />
              <Scene key="account" component={Account} />
              <Scene key="inforAccount" component={InforAccount} />
              <Scene key="news" component={News} />
              <Scene key="oder" component={Oder} />
              <Scene key='productLed' component={ProductLed} />
              <Scene key='recentOrder' component={RecentOrder} />
              <Scene key='receivedOrder' component={ReceivedOrder} />
              <Scene key='maps' component={Maps} />
              <Scene key='shippedOrder' component={ShippedOrder} />
              <Scene key='detailOrder' component={DetailOrder} />
              <Scene key='carts' component={Carts} />
            </Stack>
          </Drawer>

        </Modal>
      </Router>
    );
  }
}

